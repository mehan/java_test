package java_test01;

import java.util.Arrays;

public class demo_04_px {

	public static void main(String[] args) {
		
/*	 选择排序
		int arr[] = { 2, 5, 55, 6, 888, 98, 7, 85, 77 };
		for (int i = 1; i < arr.length; i++) {
			int index = 0;
			for (int j = 1; j <= arr.length - i; j++) {
				if (arr[index] <arr[j]) {

					index = j;
				}
			}

			int temp = arr[arr.length - i];
			arr[arr.length - i] = arr[index];
			arr[index] = temp;

		}
		System.out.println("选择排序的结果是：");
		for (int i : arr) {
			System.out.println(i + "");
		}
	*/
		/* 冒泡排序
		int arr[] = { 2, 5, 55, 6, 888, 98, 7, 85, 77 };
		for(int i=1;i<=arr.length;i++) {
			for(int j=0;j<arr.length-i;j++) {
				if(arr[j]>arr[j+1]) {
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				
				}
			}
		}
		System.out.println("冒泡排序");
		for(int tem:arr) {
			System.out.println(tem+"");
		}*/
		
		//array.sort
		int arr[] = { 2, 5, 55, 6, 888, 98, 7, 85, 77 };
		Arrays.sort(arr);
		for(int tem:arr) {
			System.out.println(tem+"");
		}
	}
}
