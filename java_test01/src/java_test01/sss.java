package java_test01;
	import java.awt.*;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.util.Random;

	import javax.swing.*;

	public class sss extends JFrame implements ActionListener{

	    private JButton [] moleButtons; // 按钮数组
	    private Timer timer; // 时间器
	    private int score = 0; // 得分
	    private JLabel scoreLabel; // 显示得分的标签

	    public sss(){
	        super("打地鼠游戏"); 

	        setLayout(new BorderLayout()); 

	        JPanel topPanel = new JPanel(); 
	        scoreLabel = new JLabel("分数: 0"); 
	        topPanel.add(scoreLabel); 
	        add(topPanel, BorderLayout.NORTH); 

	        moleButtons = new JButton[9]; 
	        JPanel centerPanel = new JPanel(new GridLayout(3, 3)); 
	        for(int i=0; i<moleButtons.length; i++){
	            moleButtons[i] = new JButton();
	            moleButtons[i].addActionListener(this); 
	            centerPanel.add(moleButtons[i]);
	        }
	        add(centerPanel, BorderLayout.CENTER);

	        timer = new Timer(1000, this); 
	    }

	    public void startGame(){
	        score = 0;
	        updateScore();
	        timer.start();
	    }

	    public void actionPerformed(ActionEvent e) {
	        
	        if(e.getSource() instanceof JButton){ 
	            
	            if(((JButton) e.getSource()).getText().equals("打到了")){ 
	            
	                score += 10;
	                updateScore();
	                ((JButton) e.getSource()).setText("");  
	            }
	            
	            return;
	            
	        }else{ 
	        
	            Random random = new Random(); 
	            int index = random.nextInt(9);
	            
	            for(int i=0; i<9; i++){
	                Button button = moleButtons[i]; 
	                if(index != i && button.getText().equals("打到了")){
	                    return;
	                }
	            }
	            
	            moleButtons[index].setText("地鼠"); 
	        }
	    }

	    private void updateScore(){
	        scoreLabel.setText("分数: " + score);
	    }

	    public static void main(String[] args) {

	        WhackAMole game = new WhackAMole(); 
	        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	        game.setSize(400, 400); 
	        
	        JPanel bottomPanel = new JPanel(); 
	        JButton startButton = new JButton("开始游戏"); 
	        startButton.addActionListener(new ActionListener(){
	            public void actionPerformed(ActionEvent e){
	                game.startGame();
	            }
	        });
	        
	        bottomPanel.add(startButton); 
	        
	        game.add(bottomPanel, BorderLayout.SOUTH);
	        
	        game.setVisible(true); 

	    }

	}

