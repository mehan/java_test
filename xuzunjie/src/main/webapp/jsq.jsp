<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>计算器</title>
</head>
<body>
	<h1>计算器</h1>

	<form method="post">
	<!--输入第一个值，并使用required并判断输入不能为空-->
		<input type="number" name="num1" required>
		<!--使用下拉列表选择相应的运算，并使用required并判断输入不能为空-->
		<select name="ys" required>
			<option value="+">+</option>
			<option value="-">-</option>
			<option value="*">*</option>
			<option value="/">/</option>
		</select>
			<!--输入第二个值，并使用required并判断输入不能为空-->
		
		<input type="number" name="num2" required>
		<input type="submit" value="计算">
	</form>

	<%
		if(request.getMethod().equals("POST")){
			int num1 = Integer.parseInt(request.getParameter("num1"));
			int num2 = Integer.parseInt(request.getParameter("num2"));
			String ys = request.getParameter("ys");
			int sum = 0;
			//使用switch判断使用选择的运算方式，并进行相应的计算
			if(num2==0){
			out.println("除数为零，不能计算"  );}else{
			
			switch(ys){
				case "+":
					sum = num1 + num2;
					break;
				case "-":
					sum = num1 - num2;
					break;
				case "*":
					sum = num1 * num2;
					break;
				case "/":
				
					sum = num1 / num2;
					break;
			}
			out.println("结果是：" + sum );
		}}
	%>

</body>
</html>