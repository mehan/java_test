<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>问卷调查</title>
</head>
<body>
<h1>问卷调查</h1>
<p>请回答下列问题：</p >
<form method="get" action="result.jsp">
<p>1. 你觉得今天天气怎么样？</p >
<input type="radio" name="q1" value="晴天">A. 晴天
<input type="radio" name="q1" value="多云">B. 多云
<input type="radio" name="q1" value="下雨">C. 下雨
<br>
<p>2. 你喜欢什么颜色？</p >
<input type="checkbox" name="q2" value="红色">A. 红色
<input type="checkbox" name="q2" value="蓝色">B. 蓝色
<input type="checkbox" name="q2" value="绿色">C. 绿色
<br>
<p>3. 你喜欢什么类型的电影？</p >
<select name="q3">
<option value="">请选择</option>
<option value="动作片">动作片</option>
<option value="爱情片">爱情片</option>
<option value="喜剧片">喜剧片</option>
</select>
<br>
<p>4. 你平时喜欢做什么？</p >
<textarea name="q4"></textarea>
<br>
<p>5. 你的性别是？</p >
<input type="radio" name="q5" value="男">男
<input type="radio" name="q5" value="女">女
<br>
<input type="submit" value="提交">
</form>
</body>
</html>
