<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%
//连接数据库
String driver = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://localhost:3306/database1";
String user = "root";
String password = "root";
//try catch 语句判断出现的异常
try {
	Class.forName(driver);
	Connection con = DriverManager.getConnection(url, user, password);
	Statement stmt = con.createStatement();
	String sql = "SELECT * FROM table1";
	ResultSet rs = stmt.executeQuery(sql);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>数据表</title>
</head>
<body>
	<table border="1">
		<tr>
			<th>姓名</th>
			<th>性别</th>
			<th>电话</th>
			<th>住址</th>
			<th>班级</th>
			<th>学号</th>
			<th>java_ee成绩</th>
		</tr>
		<!-- while(rs.next())表示遍历ResultSet中的所有行
（用next()方法逐行读取），在当前行可读取的情况下，执行表格中的输出操作， -->
		<%
		while (rs.next()) {
		%>
		<tr>
			<td><%=rs.getString("name")%></td>
			<td><%=rs.getString("gender")%></td>
			<td><%=rs.getString("phone")%></td>
			<td><%=rs.getString("address")%></td>
			<td><%=rs.getString("class")%></td>
			<td><%=rs.getString("student_id")%></td>
			<td><%=rs.getString("javaee_score")%></td>
		</tr>
		<%
		}
		%>
	</table>
	<%
	//关闭连接
	rs.close();
	stmt.close();
	con.close();
	} catch (ClassNotFoundException e) {//当遇见类异常，输出类失败
	out.println("无法找到驱动类！");
	} catch (SQLException e) {//遇见sql异常，输出数据库连接失败
	out.println("数据库连接失败！");
	}
	%>
</body>
</html>
