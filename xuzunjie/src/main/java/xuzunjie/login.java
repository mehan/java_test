package xuzunjie;

import java.io.IOException;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class login
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/* protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        String username = request.getParameter("username");
	        String password = request.getParameter("password");
	        
	        // 简单的用户名密码验证
	        if(username == null || password == null){
	            // 用户名或密码为空，跳转到错误页面
	            request.setAttribute("message", "用户名或密码不能为空！");
	            request.getRequestDispatcher("error.jsp").forward(request, response);
	        } else if("xuzunjie".equals(username) && "mayangchao".equals(password)){
	            // 验证通过，跳转到成功页面
	            response.sendRedirect("success.jsp");
	        }else{
	            // 验证失败，跳转到失败页面
	            request.getRequestDispatcher("failure.jsp").forward(request, response);
	        }
	    }
	}*/
	  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        String username = request.getParameter("username");
	        String password = request.getParameter("password");
	      
	        String url = "jdbc:mysql://localhost:3306/database1";
	        String dbUsername = "root";
	        String dbPassword = "root";
	        String driverName = "com.mysql.jdbc.Driver";

	        try {
	            Class.forName(driverName); // 加载数据库驱动
	            Connection conn = DriverManager.getConnection(url, dbUsername, dbPassword); // 获取数据库连接
	            String sql = "SELECT * FROM users WHERE username = ? AND password = ?";
	            PreparedStatement stmt = conn.prepareStatement(sql); // 预处理SQL语句
	            stmt.setString(1, username);
	            stmt.setString(2, password);
	            ResultSet rs = stmt.executeQuery(); // 执行查询操作

	            if (rs.next()) { // 用户名和密码存在于数据库中
	                HttpSession session = request.getSession();
	                session.setAttribute("username", username); // 将用户名存入session
	                response.sendRedirect("success.jsp"); // 跳转至成功页面
	            } else { // 用户名或密码不存在于数据库中
	                response.sendRedirect("error.jsp"); // 跳转至失败页面
	            }
	       
	        } catch (SQLException e) {//数据库异常
	            e.printStackTrace();
	        } catch (ClassNotFoundException e) {//类异常
				e.printStackTrace();
			}
	    }
	}