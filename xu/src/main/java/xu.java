

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class xu
 */
@WebServlet("/xu")
public class xu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public xu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        String username = request.getParameter("username");
	        String password = request.getParameter("password");
	        
	        // 简单的用户名密码验证
	        if(username == null || password == null){
	            // 用户名或密码为空，跳转到错误页面
	            request.setAttribute("message", "用户名或密码不能为空！");
	            request.getRequestDispatcher("error.jsp").forward(request, response);
	        } else if("zhangsan".equals(username) && "zhangsan".equals(password)){
	            // 验证通过，跳转到成功页面
	            response.sendRedirect("success.jsp");
	        }else{
	            // 验证失败，跳转到失败页面
	            request.getRequestDispatcher("failure.jsp").forward(request, response);
	        }
	    }

}
